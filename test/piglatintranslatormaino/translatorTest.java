package piglatintranslatormaino;

import static org.junit.Assert.*;

import org.junit.Test;

public class translatorTest {

	@Test
	public void testInputPhrase() {
		//Arrange
		String inputPhrase = "Hello world";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals("Hello world",transaltor.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		//Arrange
		String inputPhrase = "";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals(Translator.NIL,transaltor.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithY() {
		//Arrange
		String inputPhrase = "ANY";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals("ANYNAY",transaltor.translate());
	}	
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		//Arrange
		String inputPhrase = "utility";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals("utilitynay",transaltor.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVawel() {
		//Arrange
		String inputPhrase = "Apple";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals("Appleyay",transaltor.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		//Arrange
		String inputPhrase = "ask";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals("askay",transaltor.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithASingleConsonant() {
		//Arrange
		String inputPhrase = "hello";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals("ellohay",transaltor.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithMultipleConsonant() {
		//Arrange
		String inputPhrase = "known";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals("ownknay",transaltor.translate());
	}
	
	@Test
	public void testTranslationPhraseWithMultipleWords() {
		//Arrange
		String inputPhrase = "Known Hello";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals("Ownknay Ellohay",transaltor.translate());
	}
	
	@Test
	public void testTranslationPhraseWithMultipleWordsWithDash() {
		//Arrange
		String inputPhrase = "known-Hello";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals("ownknay-Ellohay",transaltor.translate());
	}
	
	@Test
	public void testTranslationPhraseWithMultipleWordsWithPunctuations() {
		//Arrange
		String inputPhrase = "helLo World!";
		//Act
		Translator transaltor = new Translator(inputPhrase);
		//Assert
		assertEquals("ellohay Orldway!",transaltor.translate());
	}
	
}//FINE DELLA CLASSE DI TESTING


