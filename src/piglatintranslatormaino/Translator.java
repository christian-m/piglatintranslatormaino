package piglatintranslatormaino;

import java.util.ArrayList;
import java.util.Collections;

public class Translator {
	public static final String NIL = "nil";
	private String phrase;
	
	public Translator(String inputPhrase) {
		this.phrase = inputPhrase;
	}
	
	public String getPhrase()
	{
		return this.phrase;
	}

	public String translate() 
	{
	
		ArrayList<String> temp = (ArrayList<String>) phraseToWordsList().clone();
		
		String ritorno= "", momentaneo="";
		boolean firstLetter, remainLetters=true;
		for(int k=0;k<temp.size();k++)
		{
			this.phrase = temp.get(k);
			
			if(!this.phrase.isEmpty())
			{
				if(punctuationsChecker(this.phrase))
				{
					ritorno += this.phrase;
				}
				else if(startWithVowel())
				{
					if(isUpper(this.phrase.charAt(0)+""))
					{
						firstLetter = true;
						for(int m=1;m<this.phrase.length();m++)
						{
							if(isUpper(this.phrase.charAt(m)+"")) remainLetters = true;
							else 
								{
									remainLetters=false;
									break;
								}
						}
						
						if(firstLetter == true && remainLetters == false)
						{
							this.phrase = this.phrase.toLowerCase();
							momentaneo = this.phrase.substring(0,1).toUpperCase() +  this.phrase.substring(1);
							if( this.phrase.endsWith("y") ) ritorno += momentaneo + "nay";
							else if( endWithVowel() ) 	  ritorno += momentaneo  + "yay";
							else if( !endWithVowel() ) 	  ritorno += momentaneo  + "ay";
						}
						else if(firstLetter == true && remainLetters == true)
						{
							momentaneo = this.phrase.toUpperCase();
							if( this.phrase.endsWith("Y") ) ritorno += momentaneo + "NAY";
							else if( endWithVowel() ) 	  ritorno += momentaneo  + "YAY";
							else if( !endWithVowel() ) 	  ritorno += momentaneo  + "AY";
						}
					}
					else 
					{
						momentaneo = this.phrase.toLowerCase();
						if( this.phrase.endsWith("y") ) ritorno += momentaneo + "nay";
						else if( endWithVowel() ) 	  ritorno += momentaneo  + "yay";
						else if( !endWithVowel() ) 	  ritorno += momentaneo  + "ay";
						
					}
					
				}
				else if(!startWithVowel()) 
				{
					if(isUpper(this.phrase.charAt(0)+""))
					{
						firstLetter = true;
						for(int m=1;m<this.phrase.length();m++)
						{
							if(isUpper(this.phrase.charAt(m)+"")) remainLetters = true;
							else 
								{
									remainLetters=false;
									break;
								}
						}
						if(firstLetter == true && remainLetters == false)
						{
							for(int i=1 ; i<this.phrase.length() ; i++)
							{
								if(vowelChecker(this.phrase.charAt(i)+"")) 
									{
										ritorno += this.phrase.substring(i,i+1).toUpperCase() + this.phrase.substring(i+1).toLowerCase() + this.phrase.substring(0,i).toLowerCase() + "ay";
										break;
									} 
							}
						}
						else if(firstLetter == true && remainLetters == true)
						{
							for(int i=1 ; i<this.phrase.length() ; i++)
							{
								if(vowelChecker(this.phrase.charAt(i)+"")) 
									{
										ritorno += this.phrase.substring(i).toUpperCase() + this.phrase.substring(0,i).toUpperCase() + "AY";
										break;
									} 
							}
						}
					}
					else 
					{
						for(int i=1 ; i<this.phrase.length() ; i++)
						{
							if(vowelChecker(this.phrase.charAt(i)+"")) 
								{
									ritorno += this.phrase.substring(i).toLowerCase() + this.phrase.substring(0,i).toLowerCase() + "ay";
									break;
								} 
						}
						
					}
				}
			}
			else
			{
				ritorno += NIL;
			}
		}
		return ritorno;
	}
	
	
	
	private boolean startWithVowel()
	{
		return(phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u") || phrase.startsWith("A") || phrase.startsWith("E") || phrase.startsWith("I") || phrase.startsWith("O") || phrase.startsWith("U"));
	}
	
	private boolean endWithVowel()
	{
		return(phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	private boolean vowelChecker(String letter)
	{
		String vowel ="aeiouAEIOU";
		return vowel.contains(letter);
	}
	
	private boolean alphabetChecker(String letter)
	{
		String alphabet ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		return alphabet.contains(letter);
	}
	
	private boolean punctuationsChecker(String field)
	{
		String punctuations =".-;,?!'() ";
		return punctuations.contains(field);
	}
	
	private boolean isUpper(String field)
	{
		String punctuations ="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		return punctuations.contains(field);
	}
	
	private ArrayList<String> phraseToWordsList()
	{
		ArrayList<String> words = new ArrayList<String>();
		String parola="";
		
		if(this.phrase.isEmpty())
		{
			words.add(parola);
		}
		else
		{
			for(int i=0;i<=this.phrase.length();i++)
			{
				if(i != this.phrase.length())
				{
					if(alphabetChecker(this.phrase.charAt(i)+""))
					{
						parola += this.phrase.charAt(i);
					}
					else if(punctuationsChecker(this.phrase.charAt(i)+""))
					{
						if(!parola.isEmpty())
						{
							words.add(parola);
							words.add(this.phrase.substring(i,i+1));
							parola="";
							if(i+1 == this.phrase.length())
							{
								break;
							}
						}
						else
						{
							words.add(this.phrase.substring(i,i+1));
						}
					}
				}
				else if(i == 0)
				{
					words.add(this.phrase);
					break;
				}
				else
				{
					words.add(parola);
				}
			}
		}
		
		
		return words;
	}
	
}//fine classe 


